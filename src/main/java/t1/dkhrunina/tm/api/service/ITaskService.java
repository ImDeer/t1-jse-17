package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAll(Sort sort);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}